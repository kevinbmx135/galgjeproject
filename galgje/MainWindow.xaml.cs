﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace galgje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int levens;
        int Speler = 2;
        int levensVR = 0;
        string vbWoord;
        string geradenLt;
        string ftLetters;
        int seconden;
        List<string> woord = new List<string>();
        DispatcherTimer timer = new DispatcherTimer();
        string[] woordOpBeeld;
        private string[] galgjeWoorden = new string[]
{
    "grafeem",
    "tjiftjaf",
    "maquette",
    "kitsch",
    "pochet",
    "convocaat",
    "jakkeren",
    "collaps",
    "zuivel",
    "cesium",
    "voyant",
    "spitten",
    "pancake",
    "gietlepel",
    "karwats",
    "dehydreren",
    "viswijf",
    "flater",
    "cretonne",
    "sennhut",
    "tichel",
    "wijten",
    "cadeau",
    "trotyl",
    "chopper",
    "pielen",
    "vigeren",
    "vrijuit",
    "dimorf",
    "kolchoz",
    "janhen",
    "plexus",
    "borium",
    "ontweien",
    "quiche",
    "ijverig",
    "mecenaat",
    "falset",
    "telexen",
    "hieruit",
    "femelaar",
    "cohesie",
    "exogeen",
    "plebejer",
    "opbouw",
    "zodiak",
    "volder",
    "vrezen",
    "convex",
    "verzenden",
    "ijstijd",
    "fetisj",
    "gerekt",
    "necrose",
    "conclaaf",
    "clipper",
    "poppetjes",
    "looikuip",
    "hinten",
    "inbreng",
    "arbitraal",
    "dewijl",
    "kapzaag",
    "welletjes",
    "bissen",
    "catgut",
    "oxymoron",
    "heerschaar",
    "ureter",
    "kijkbuis",
    "dryade",
    "grofweg",
    "laudanum",
    "excitatie",
    "revolte",
    "heugel",
    "geroerd",
    "hierbij",
    "glazig",
    "pussen",
    "liquide",
    "aquarium",
    "formol",
    "kwelder",
    "zwager",
    "vuldop",
    "halfaap",
    "hansop",
    "windvaan",
    "bewogen",
    "vulstuk",
    "efemeer",
    "decisief",
    "omslag",
    "prairie",
    "schuit",
    "weivlies",
    "ontzeggen",
    "schijn",
    "sousafoon"
};
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnRaad_Click(object sender, RoutedEventArgs e)
        {
            controle(TxtWoord.Text);
            TxtWoord.Clear();
            seconden = 10;
            lblTimer.Content = seconden.ToString();
            
        }
        private void controle(string geradenWoord)
        {
            int juistGR = 0;
            if (TxtWoord.Text.Length > 1)
            {
                if (vbWoord == geradenWoord)
                {
                    string tekst = "Hoera!!!";
                    tekst += Environment.NewLine;
                    tekst += $"je hebt ";
                    tekst += Environment.NewLine;
                    tekst += $"'{vbWoord}' ";
                    tekst += Environment.NewLine;
                    tekst += $"correct geraden  ";
                    lblMelding.Content = tekst;
                    timer.Stop();
                }
                else
                {
                    levens--;
                    levensVR++;
                    levensOp();
                    uitvoer();


                   
                }
               
                
              
            }
            if (TxtWoord.Text.Length == 1)
            {
                for (int i = 0; i < woord.Count; i++)
                {
                    if (woord[i] == geradenWoord )
                    {
                        geradenLt += woord[i];
                        woordOpBeeld[i] = woord[i] ;
                        uitvoer();
                        juistGR = 1;
                    }
                    
                }
                if (juistGR == 0)
                {
                    levens--;
                    levensVR++;
                    ftLetters += geradenWoord;
                    
                    uitvoer();
                    levensOp();
                }
            }
        }
        private void levensOp()
        {
           
            if (levens == 0 )
            {
                string tekst = "je hebt het geheim woord ";
                tekst += Environment.NewLine;
                tekst += $"niet geraden ";
                tekst += Environment.NewLine;
                tekst += $"Je bent opgehangen!  ";
                lblMelding.Content = tekst;
                timer.Stop();
            }
           
        }
        private void uitvoer()
        {
            seconden = 11;
            string lengtWoord = null;
            for (int i = 0; i < woordOpBeeld.GetLength(0); i++)
            {
                lengtWoord += woordOpBeeld[i];
            }
            string tekst;
            tekst = $"{levens} levens";
            tekst += Environment.NewLine;
            tekst += $"Juiste letters: {geradenLt}";
            tekst += Environment.NewLine;
            tekst += $"Foute Letters :  {ftLetters}";
            tekst += Environment.NewLine;
            tekst += $"  {lengtWoord}";
            lblMelding.Content = tekst;
            TekennenMannetje();
            BtnRaad.IsEnabled = true;
        }

        private void BtnNieuwpel_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            levens = 11;
            levensVR = 0;
            
                woord.Clear();
            
            StartGame();
        }

        private void MainWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            levens = 10;
            seconden = 11;
            

           
            StartGame();

        }
        private void StartGame()
        {
            if (Speler == 1)
            {
                BtnVerbergWoord.Content = "start Spel";
            }
            else
            {
                //variablen 
                string Tekst;

                //berwerking
                Tekst = "Geef een geheim woord in ";

                //uitvoer
                BtnVerbergWoord.Content = "Verberg Woord";
                lblMelding.Content = Tekst;
            }
            

            //stuk wat zwz moet gebeuren 
            BtnNieuwpel.Visibility = Visibility.Hidden;
            BtnRaad.IsEnabled = false;
            BtnVerbergWoord.Visibility = Visibility.Visible;

        }

        private void BtnVerbergWoord_Click(object sender, RoutedEventArgs e)
        {
            if (Speler == 1)
            {
                int WelkWd;
                Random radom = new Random();
                WelkWd = radom.Next(0, galgjeWoorden.GetLength(0) + 1);
                vbWoord = galgjeWoorden[WelkWd];
            }
            else
            {
                
                vbWoord = TxtWoord.Text;
               
            }
            for (int i = 0; i <vbWoord.Length; i++)
            {

                woord.Add(vbWoord.Substring(i, 1));
            }


            BtnVerbergWoord.Visibility = Visibility.Hidden;
            BtnNieuwpel.Visibility = Visibility.Visible;
            BtnRaad.IsEnabled = true;
            TxtWoord.Text = "";

            //timer maken en starten 
           
            timer.Tick += new EventHandler(DispatcherTimer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
            //aantal streepyes voor te weten hoelang het woord is 
            woordOpBeeld = new string[woord.Count];
            for (int i = 0; i < woord.Count; i++)
            {
                woordOpBeeld[i] = "*";

            }
            uitvoer();


        }
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (seconden == 0)
            {
                levens--;
                levensVR++;
                uitvoer();
                MessageBox.Show("je hebt een leven af omfat je te lang wacht je hebt maar 10secodnen ");
                seconden = 11;
            }
            seconden--;
            
            lblTimer.Content = seconden.ToString();

        }

        

        private void MnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MnuSingelplayer_Click(object sender, RoutedEventArgs e)
        {
            Speler = 1;
            StartGame();
        }

        private void MnuTweeSpelers_Click(object sender, RoutedEventArgs e)
        {
            Speler = 2;
            StartGame();
        }
        private void TekennenMannetje()
        {
            Uri fotoOphalen = new Uri($"foto's/galgje-{levensVR}.png", UriKind.Relative);
            BitmapImage foto = new BitmapImage(fotoOphalen);
            cnvImgs.Source = foto;
        }
    }
}
